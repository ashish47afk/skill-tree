import React from 'react';
import Tree from 'react-d3-tree';
import './App.css';
 
const myTreeData = [
  {
    name: 'Full Stack',
    attributes: {
          keyA: <a href="https://www.facebook.com">Link 1</a>,
          keyB: <a href="https://www.instagram.com">Link 2</a>,
          keyC: <a href="https://www.google.com">Link 3</a>,
    },
    children: [
      {
        name: 'Front End',
        attributes: {
          keyA: <a href="https://www.facebook.com">Link 1</a>,
          keyB: <a href="https://www.instagram.com">Link 2</a>,
          keyC: <a href="https://www.google.com">Link 3</a>,
        },
        children: [
          {
            name: 'React',
            attributes: {
              keyA: <a href="https://www.facebook.com">Link 1</a>,
              keyB: <a href="https://www.instagram.com">Link 2</a>,
              keyC: <a href="https://www.google.com">Link 3</a>,
            },
            children: [
              {
                name: 'React JS',
                attributes: {
                  keyA: <a href="https://www.facebook.com">Link 1</a>,
                  keyB: <a href="https://www.instagram.com">Link 2</a>,
                  keyC: <a href="https://www.google.com">Link 3</a>,
                },
                children: [
                  {
                    name: 'React Native',
                    attributes: {
                      keyA: <a href="https://www.facebook.com">Link 1</a>,
                      keyB: <a href="https://www.instagram.com">Link 2</a>,
                      keyC: <a href="https://www.google.com">Link 3</a>,
                    },
                  },
                ]
              },
            ]
          },
          {
            name: 'Angular',
            attributes: {
              keyA: <a href="https://www.facebook.com">Link 1</a>,
              keyB: <a href="https://www.instagram.com">Link 2</a>,
              keyC: <a href="https://www.google.com">Link 3</a>,
            },
           
          },
        ]
      },
      {
        name: 'Back End',
        attributes: {
              keyA: <a href="https://www.facebook.com">Link 1</a>,
              keyB: <a href="https://www.instagram.com">Link 2</a>,
              keyC: <a href="https://www.google.com">Link 3</a>,
        },
        children: [
          {
            name: 'Spring Boot',
            attributes: {
              keyA: <a href="https://www.facebook.com">Link 1</a>,
              keyB: <a href="https://www.instagram.com">Link 2</a>,
              keyC: <a href="https://www.google.com">Link 3</a>,
            },
            children: [
              {
                name: 'Sample Data(1)',
                attributes: {
                  keyA: <a href="https://www.facebook.com">Link 1</a>,
                  keyB: <a href="https://www.instagram.com">Link 2</a>,
                  keyC: <a href="https://www.google.com">Link 3</a>,
                },
                children: [
                  {
                    name: 'Sample Data(Final)',
                    attributes: {
                      keyA: <a href="https://www.facebook.com">Link 1</a>,
                      keyB: <a href="https://www.instagram.com">Link 2</a>,
                      keyC: <a href="https://www.google.com">Link 3</a>,
                    },
                  },
                ]
              },
            ]
          },
        ]
      },
    ],
  },
];
const svgSquare = {
  shape: 'rect',
  shapeProps: {
    width: 10,
    height: 10,
    x: -5,
    y: -5,
    fill: '#FFA500',
  }
}
const style = {
  links:{
    fill: "none",
    stroke: "yellow",
    strokeWidth: "5px",
    strokeDashArray: "2,2",
  }
}
const tran = {
  x: 500,
  y: 400
}

class App extends React.Component {
  render() {
    return (
      <div className="treeWrapper" style={{width: '665em', height: '200em'}}>
 
 <Tree data={myTreeData} pathFunc="elbow" nodeSvgShape={svgSquare} styles={style} translate={tran}/>
 
      </div>
    );
  }
}
export default App;